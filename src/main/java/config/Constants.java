package config;

public class Constants {

    public static final String PROPERTIES_PATH = System.getProperty("user.dir") + "\\src\\main\\java\\config\\config.properties";
    public static long IMPLICIT_WAIT = 10;
    public static long PAGE_LOAD_TIMEOUT = 15;
    public static String TESTDATA_SHEET_PATH = "*.com/app/qa/testData/AppTestData.xlsx";

}
